import {Column, Model, Table} from "sequelize-typescript";

@Table
export class AuthorModel extends  Model {
    @Column
    firstName: string;

    @Column
    lastName: string;

    @Column
    age: number;
}
