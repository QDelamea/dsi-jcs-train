import { app } from "./app";
import {sequelize} from "./sequelize";

app.listen(3000, () => {
    console.log('App is listening on port 3000');
});

sequelize.sync().then(() => {
    console.log('Successfully connected to the database');
}).catch((error) => {
    console.log(error)
});
