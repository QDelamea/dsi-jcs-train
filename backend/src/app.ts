import express from 'express';
import {apiRouter} from "./route";

export const app = express();

app.use(express.urlencoded({ extended: true}));
app.use(express.json());

app.use('/api', apiRouter);

app.use('*', (req: express.Request, res: express.Response) => {
    res.status(404).send('This route doesn\'t exist');
});
