export interface CreateAuthorInterface {
    firstName: string;
    lastName: string;
    age: number;
}

export interface UpdateAuthorInterface {
    age: number;
}
