import { Sequelize } from "sequelize-typescript";
import { AuthorModel } from "./model/author.model";

export const sequelize = new Sequelize({
    dialect: 'mysql',
    database: process.env.MYSQL_DATABASE,
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    host: process.env.DB_HOST,
    logging: false
});

sequelize.addModels([AuthorModel]);
