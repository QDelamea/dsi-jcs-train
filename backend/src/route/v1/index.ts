import { Router } from "express";
import {authorRouter} from "./author.route";

export const v1Router = Router();

v1Router.use('/author', authorRouter);
