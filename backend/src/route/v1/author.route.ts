import { Router, Request, Response } from "express";
import {AuthorModel} from "../../model/author.model";
import {CreateAuthorInterface, UpdateAuthorInterface} from "../../interfaces/author.interface";

export const authorRouter = Router();

authorRouter.get('/get-all', async (req: Request, res: Response) => {
    const authors = await AuthorModel.findAll();
    if (!authors) {
        res.status(404).send('No result');
    }
    res.status(200).send(authors);
});

authorRouter.get('/:id', async (req: Request, res: Response) => {
    const authorId = parseInt(req.params.id, 10);
    const author = await AuthorModel.findOne({where: {id: authorId}});
    if (!author) {
        res.status(404).send(`No author with id ${authorId}`)
    }
    res.status(200).send(author);
});

authorRouter.post('/:id/delete', async (req: Request, res: Response) => {
    const authorId = parseInt(req.params.id, 10);
    const author = await AuthorModel.findOne({where: {id: authorId}});
    if (!author) {
        res.status(404).send(`No author with id ${authorId}`)
    }
    await author?.destroy();
    res.status(200).send(`Deleted author id ${authorId}`);
});

authorRouter.post('/:id/update', async (req: Request, res: Response) => {
    const authorId = parseInt(req.params.id, 10);
    const reqData = req.body as UpdateAuthorInterface;
    const updatedAuthor = await AuthorModel.update({age: reqData.age}, {where: {id: authorId}});
    if (!updatedAuthor) {
        res.status(404).send(`No author with id ${authorId}`)
    }
    res.status(200).send(updatedAuthor);
});

authorRouter.post('/create', async (req: Request, res: Response) => {
    const reqData = req.body as CreateAuthorInterface;
    const newAuthor = await AuthorModel.create({
        firstName: reqData.firstName,
        lastName: reqData.lastName,
        age: reqData.age
    });
    res.status(201).send(newAuthor);
});
